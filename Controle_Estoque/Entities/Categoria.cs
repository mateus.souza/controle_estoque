﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controle_Estoque.Entities
{
    public class Categoria
    {
        private string Descricao;

        public Categoria(string descricao)
        {
            Descricao = descricao;
        }

        public override string ToString()
        {
            return "Descrição: " + Descricao;
        }
    }
}
