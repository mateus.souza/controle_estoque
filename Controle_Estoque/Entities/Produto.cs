﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controle_Estoque.Entities
{
    public class Produto
    {
        public int Codigo { get; private set; }
        private string Descricao;
        private double PrecoCusto;
        public double PrecoVenda { get; set; }
        public int QuantidadeEstoque { get;  set; }
        private Categoria _categoria;
        public List<Produto> lista = new List<Produto>();

        public Produto(int codigo, string descricao, double precoCusto, double precoVenda, int quantidadeEstoque, Categoria categoria)
        {
            Codigo = codigo;
            Descricao = descricao;
            PrecoCusto = precoCusto;
            PrecoVenda = precoVenda;
            QuantidadeEstoque = quantidadeEstoque;
            _categoria = categoria;
            
        }

        public Produto()
        {

        }

        public void AddProduto(int quantidade)
        {
            ValidarQuantidadadeAdd(quantidade);
            QuantidadeEstoque += quantidade;
        }

        private void ValidarQuantidadadeAdd(int quantidade)
        {
            if(quantidade <= 0)
            {
                throw new ArgumentException("Adicione uma quantidade válida!");
            }
        }

        public void RemoveProduto(int quantidade)
        {
            ValidarQuantidadadeRemovida(quantidade);
            QuantidadeEstoque -= quantidade;
        }

        private void ValidarQuantidadadeRemovida(int quantidade)
        {
            if(quantidade > QuantidadeEstoque)
            {
                throw new ArgumentException("Quantidade inválida para remoção!");
            }
        }



        public override string ToString()
        {
            return "Codigo: " + Codigo +
                "\n Descrição: " + Descricao +
                "\n Preço de Custo: " + PrecoCusto +
                "\n Quantidade em estoque: " + QuantidadeEstoque +
                "\n Categoria: " + _categoria.ToString(); 

        }
    }
}
