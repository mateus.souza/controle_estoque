﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controle_Estoque.Entities
{
    public class Pedido
    {
        private int Numero;
        private DateTime Data;
        private Produto _produto;
        private Item _item;

        public Pedido(int numero, DateTime data)
        {
            Numero = numero + 1;
            Data = data;
        }

        public void EmitirPedido(int quantidade)
        {
            _produto.RemoveProduto(quantidade);
        }

        public override string ToString()
        {
            return "Numero: " +Numero +
                "Data: " + Data.ToString("dd/MM/yyyy") + 
                "Produto: " + _produto.Codigo + 
                "Quantidade: " + _item.Quantidade;
        }
    }
}
