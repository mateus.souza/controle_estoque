﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Controle_Estoque.Entities
{
    public class Item
    {
        public int Quantidade { get; set; }
        private double ValorUnitario;
        Produto Produto;
        

        public Item(int quantidade, double valorUnitario)
        {
            Quantidade = quantidade;
            ValorUnitario = valorUnitario;
        }

        public Item()
        {

        }

        public void VenderItem(Produto produto, int quantidade, double valorUnitario)
        {
             produto.RemoveProduto(quantidade);
             produto.PrecoVenda = valorUnitario;
        }

       
    }
}
