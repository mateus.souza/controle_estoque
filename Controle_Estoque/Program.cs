﻿using Controle_Estoque.Entities;
using System;
using System.Collections.Generic;

namespace Controle_Estoque
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Produto produto;
            List<Produto> list = new List<Produto>();

            Console.WriteLine("------------------Seja Bem vindo----------------------!");
            Console.WriteLine("1. Cadastar Produtos");
            Console.Write("O que deseja fazer: ");
            int opcao = int.Parse(Console.ReadLine());

            Console.WriteLine();

            while(opcao == 1)
            {
                Console.Write("Código: ");
                int codigo = int.Parse(Console.ReadLine());
                Console.Write("Descrição: ");
                string nome = Console.ReadLine();
                Console.Write("Informe o preço de custo: ");
                float precoCusto = float.Parse(Console.ReadLine());
                Console.Write("Informe o preço de venda: ");
                float precoVenda = float.Parse(Console.ReadLine());
                Console.Write("Informe a quantidade em estoque: ");
                int quantidadeEstoque = int.Parse(Console.ReadLine());
                Console.Write("Informe a categoria do produto: ");
                string categoria = Console.ReadLine();

                produto = new Produto(codigo, nome, precoCusto, precoVenda, quantidadeEstoque, new Categoria(categoria));
                list.Add(produto);

                Console.Write("Deseja cadastrar outro produto? (S/N): ");
                char continuar = char.Parse(Console.ReadLine());

                if(continuar == 'S' || continuar == 's')
                {
                    Console.WriteLine();
                    continue;
                }

                Console.WriteLine();
                Console.WriteLine("2. Adicionar Produtos");
                Console.WriteLine("3. Remover Produtos");
                Console.WriteLine("4. Vender Produtos");
                Console.Write("O que deseja fazer: ");
                opcao = int.Parse(Console.ReadLine());
                while (opcao == 2)
                {
                    foreach (var produtos in list)
                    {
                        Console.WriteLine();
                        Console.WriteLine(produtos);
                        Console.WriteLine();

                        Console.Write("Informe o codigo do produto: ");
                        int codigoProduto = int.Parse(Console.ReadLine());

                        if (!produtos.Codigo.Equals(codigoProduto))
                        {
                            Console.WriteLine("Esse produto não existe! ");
                            break;
                        }
                        Console.Write("Informe a quantidade a ser adicionada: ");
                        int quantidadeAdd = int.Parse(Console.ReadLine());
                        if(quantidadeAdd <= 0)
                        {
                            throw new Exception("Digite uma quantidade válida!");
                        }
                        produto.AddProduto(quantidadeAdd);
                        Console.WriteLine($"O estoque atual do produto cod: {produtos.Codigo} é: " + produtos.QuantidadeEstoque);

                    }
                    break;
                }
            }

            

            

           






        }
    }
}
