using Xunit;
using Controle_Estoque.Entities;
using System;

namespace Teste;

public class UnitTest1
{
    [Fact]
    public void AdicionarProdutoTest()
    {
            Produto produto;

            int codigo = 1020;
            string descricao = "Notebook";
            double precoCusto = 100;
            double precoVenda = 150;
            int quantidadeEstoque = 10;
            string descricaoCategoria = "Computers";

            
            produto = new Produto(codigo, descricao, precoCusto, precoVenda, quantidadeEstoque, new Categoria(descricaoCategoria));

            int AddProduto = 100;
            int estoqueNovo = AddProduto + quantidadeEstoque;
            produto.AddProduto(AddProduto);
            Assert.Equal(estoqueNovo, produto.QuantidadeEstoque);
    }

    [Fact]
    public void RemoveProdutoTest()
    {
            Produto produto;

            int codigo = 1020;
            string descricao = "Notebook";
            double precoCusto = 100;
            double precoVenda = 150;
            int quantidadeEstoque = 10;
            string descricaoCategoria = "Computers";

            
            produto = new Produto(codigo, descricao, precoCusto, precoVenda, quantidadeEstoque, new Categoria(descricaoCategoria));

            int RmProduto = 5;
            int estoqueNovo = quantidadeEstoque - RmProduto;
            produto.RemoveProduto(RmProduto);
            Assert.Equal(estoqueNovo, produto.QuantidadeEstoque);
    }

    [Fact]
    public void EmitirPedidoTest()
    {
        Pedido pedido;
        Produto produto;
        Item item = new Item();
        
        int codigo = 1020;
        string descricao = "Notebook";
        double precoCusto = 100;
        double precoVenda = 150;
        int quantidadeEstoque = 10;
        string descricaoCategoria = "Computers";

        int codItem = 1020;
        int quantidadeItem = 2;
        double valorUnitario = 100;
        int qtdeNova = quantidadeEstoque - quantidadeItem;
        produto = new Produto(codigo, descricao, precoCusto, precoVenda, quantidadeEstoque, new Categoria(descricaoCategoria));
        
        item.VenderItem(produto, quantidadeItem, valorUnitario);
        
        Assert.Equal(produto.QuantidadeEstoque, qtdeNova);
    }
}